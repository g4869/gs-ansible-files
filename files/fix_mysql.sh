# FIX MYSQL SHRINK
#!/bin/bash

echo "Creating backup directory..."
mkdir /backup

echo "Backing up MySQL data directory..."
cd /var/lib
cp -r mysql /backup

echo "Performing MySQL dump of all databases..."
mysqldump -u root --all-databases > /backup/all-database.sql

# Check if backup is successful
if [ $? -eq 0 ]; then
  echo "Backup completed successfully."
else
  echo "Backup failed. Exiting."
  exit 1
fi

# List of databases to exclude
EXCLUDED_DBS=("information_schema" "mysql")

# Delete databases (except excluded ones)
mysql -u root --skip-column-names -e "SHOW DATABASES" | while read dbname; do
  if [[ ! " ${EXCLUDED_DBS[@]} " =~ " ${dbname} " ]]; then
    mysql -u root -e "DROP DATABASE $dbname"
    echo "Deleted database: $dbname"
  fi
done


echo "Stopping MySQL service..."
service mariadb stop

echo "Removing specific MySQL data files..."
cd /var/lib/mysql/
rm ibdata1
rm ib_logfile0
rm ib_logfile1

echo "Configuring innodb_file_per_table..."
cp /etc/my.cnf /etc/my.cnf.bak
sed -i '/^\[mysqld]$/a innodb_file_per_table' /etc/my.cnf

echo "Starting MySQL service..."
service mariadb start

echo "Restoring databases from the backup..."
mysql -u root < /backup/all-database.sql

# Check if database restoration is successful
if [ $? -eq 0 ]; then
  echo "Database restoration completed successfully."
else
  echo "Database restoration failed. Exiting."
  exit 1
fi

echo "Script executed successfully."
